# README #


## PHP - Concept Básicos ##

* El lenguaje PHP es un lenguaje de programación de estilo clásico, con esto quiero decir que es un lenguaje de programación con variables, sentencias condicionales, bucles, funciones etc. 
* No es un lenguaje de marcas como podría ser HTML, XML o WML. 
* Está mas cercano a JavaScript o a C, para aquellos que conocen estos lenguajes. 
* PHP se ejecuta en el servidor, por eso nos permite acceder a los recursos que tenga el servidor como por ejemplo podría ser una base de datos. * El programa PHP es ejecutado en el servidor y el resultado enviado al navegador. 
* El resultado es normalmente una página HTML o tambie podria ser XML. 
* Al ser PHP un lenguaje que se ejecuta en el servidor no es necesario que su navegador lo soporte, es independiente del navegador, pero sin embargo para que sus páginas PHP funcionen, el servidor donde están alojadas debe soportar PHP.

### Variable
Una variable es un contenedor de información, en el que podemos meter números enteros, números decimales, carácteres... el contenido de las variables se puede leer y se puede cambiar durante la ejecución de una página PHP. En PHP todas las variables comienzan con el símbolo del dólar $ y no es necesario definir una variable antes de usarla. Tampoco tienen tipos, es decir que una misma variable puede contener un número y luego puede contener carácteres.


    <html>
    <head>
    <title>Ejemplo de PHP</title>
    </head><body>
    <?php 
    $a = 1;
    $b= 3.34 ;
     
    $c = "Hola Amigos "; 
    echo $a,"<br>",$b,"<br>",$c; 
    ?>
    </body></html>

En este ejemplo hemos definido tres variables, $a , $b y $c y con la instrucción echo hemos impreso el valor que contenían, insertando un salto de línea entre ellas. Existen dos tipos de variables, las variables locales que solo pueden ser usadas dentro de funciones y las variables globales que tienen su ámbito de uso fuera de las funciones, podemos acceder a una variable global desde una función con la instrucción global nombre_variable;

### Aritméticos

Los operadores de PHP son muy parecidos a los de C y JavaScript, si usted conoce estos lenguajes le resultaran familiares y fáciles de reconocer. Estos son los operadores que se pueden aplicar a las variables y constantes numéricas.
OperadorNombre EjemploDescripción + Suma 5 + 6 Suma dos números - Resta 7 - 9 Resta dos números * Multiplicación 6 * 3 Multiplica dos números / División 4 / 8 Divide dos números % M ódul o 7 % 2 devuelve el residuo de dividir ambos números ++ -- Ejemplo:

    <html>
    <head>
     
    Suma 1 
    Resta 1 
     
    $a++ Suma 1 al contenido de una variable. 
    $a-- Resta 1 al contenido de una variable. 
     
    <title>Ejemplo de PHP
    
    </title>
    </head><body>
    <?php 
    $a = 8; $b = 3; 
    echo $a + $b,"<br>"; echo $a - $b,"<br>"; echo $a * $b,"<br>"; echo $a / $b,"<br>"; 
    $a + + ; 
    echo $a,"<br>"; 
    $b- - ; 
    echo $b,"<br>"; 
    ?>
     
    </body></html>


### Operadores

Los operadores de comparación son usados para comparar valores y así poder tomar decisiones.
OperadorNombre EjemploDevuelve cierto cuando: == I gua l $a == $b$a es igual $b != Distinto $a != $b $a es distinto $b

    <   Menor que     $a < $b $a es menor que $b 
    >   Mayor que     $a > $b $a es mayor que $b 
    <=  Menor o igual$a <= $b $a es menor o igual que $b >=  Mayor o igual$a >= $b $a es mayor o igual que $b 

Ejemplo: 

    <html>
    <head>
    <title>Ejemplo de PHP</title>
    </head><body>
    <?php 
    $a = 8; $b = 3; $c = 3; 
    echo $a == $b,"<br>"; echo $a != $b,"<br>"; echo $a < $b,"<br>"; echo $a > $b,"<br>"; 
    echo $a >= $c,"<br>"; echo $b <= $c,"<br>"; 
    ?>
    </body></html>
    
Lógicos 

Los operadores lógicos son usados para evaluar varias comparaciones, combinando los 
posibles valores de estas. 

OperadorNombreEjemplo   Devuelve cierto cuando: 
(7>2) &&    Devuelve verdadero cuando ambas condiciones son 
 
&&
and 
 
Y
Y
 
( 2< 4) 
( 7> 2) and 
 
verdaderas. 
Devuelve verdadero cuando ambas condiciones son 
 
 
( 2< 4)     verdaderas. 
 
|| 
 
O
 
(7>2) || (2<4) Devuelve verdadero .verdadero cuando al menos una de las dos es verdadera 
 
or 
 
O
 
(7>2) or (2<4) Devuelve verdadero .verdadero cuando al menos una de las dos es verdadera 
 
!   No  ! ( 7> 2)   Niega el valor de la expresión. 

Ejemplo: 

    <html>
    <head>
    <title>Ejemplo de PHP</title>
    </head><body>
    <?php 
    $a = 8; $b = 3; $c = 3; 
    echo ($a == $b) && ($c > $b),"<br>"; 
    echo ($a == $b) || ($b == $c),"<br>"; 
    echo !($b <= $c),"<br>"; 
    ?>
    </body></html>

### Condicionales 

Las sentencias condicionales nos permiten ejecutar o no unas ciertas instrucciones dependiendo del resultado de evaluar una condición. Las más frecuentes son la instrucción if y la instrucción switch.
Sentencia if ... else

    <?php 
    if (condición) 
    {
    Sentencias a ejecutar cuando la condición es cierta. 
    }
    el se 
    {
    Sentecias a ejecutar cuando la condición es falsa. 
    }
    ?>
 

La sentencia if ejecuta una serie de instrucciones u otras dependiendo de la condición que le pongamos. Probablemente sea la instrucción más importante en cualquier lenguaje de programación.

    <html>
    <head>
    <title>Ejemplo de PHP</title>
    </head><body>
    <?php 
    $a = 8; $b = 3; 
    if ($a < $b) 
    {
    echo "a es menor que b"; 
    }
    else 
    {
    echo "a no es menor que b"; 
    }
    ?>
    </body></html>

En este ejemplo la condición no es verdadera por lo que se ejecuta la parte de código correspondiente al else .
Sentencia switch ... case

    <html>
    <head>
    <title>Ejemplo de PHP</title>
    </head><body>
    <?php 
    $posicion = "arriba"; 
    
    switch($posicion) { 
    case "arriba": // Bloque 1 
    echo "La variable contiene"; 
    echo " el valor arriba"; 
    break; 
    case "abajo": // Bloque 2 
    echo "La variable contiene"; 
    echo " el valor abajo"; 
    break; 
    default: // Bloque 3 
    echo "La variable contiene otro valor"; 
    echo " distinto de arriba y abajo"; 
     
    }
    ?>
    </body></html>

Con la sentencia switch podemos ejecutar unas u otras instrucciones dependiendo del valor de una variable, en el ejemplo anterior, dependiendo del valor de la variable $posicion se ejecuta el bloque 1 cuando el valor es "arriba", el bloque 2 cuando el valor es "abajo" y el bloque 3 si no es ninguno de los valores anteriores. Bucles Los bucles nos permiten iterar conjuntos de instrucciones , es decir repetir la ejecución de un conjunto de instrucciones mientras se cumpla una condición.
Sentencia while

    <?php 
    while (condición) 
    {
    intrucciones a ejecutar. 
    }
    ?>

Mientras la condición sea cierta se reiterará la ejecución de las instrucciones que están 
dentro del while 

Ejemplo: 

    <html>
    <head>
    <title>Ejemplo de PHP</title>
    </head><body>
    Inicio<BR>
    <?php 
    $i = 0; 
    while ($i<10) 
    {
    echo "El valor de i es ", $i,"<br>"; 
    $i + + ; 
    }
    ?>
    Final<BR>
    </body></html>
 

En el siguiente ejemplo, el valor de $i al comienzo es 0 , durante la ejecución del bucle, se va sumando 1 al valor de $i de manera que cuando $i vale 10 ya no se cumple la condición y se termina la ejecución del bucle. Sentencia for

    <?php 
    for (inicial ; condición ; ejecutar en iteración) 
    {
    intrucciones a ejecutar. 
    }
    ?>
    
Ejemplo: 
    
    <html>
    <head>
    <title>Ejemplo de PHP</title>
    </head><body>
    Inicio<BR>
    <?php 
    for($i=0 ; $i<10 ; $i++) 
    {
    echo "El valor de i es ", $i,"<br>"; 
    }
    ?>
    Final<BR>
    </body></html>

La instrucción for es la instrucción de bucles más completa. En una sola instrucción nos permite controlar todo el funcionamiento del bucle. El primer parámetro del for , es ejecutado la primera vez y sirve para inicializar la variable del bucle, el segundo parámetro indica la condición que se debe cumplir para que el bucle siga ejecutándose y el tercer parámetro es una instrucción que se ejecuta al final de cada iteración y sirve para modificar el valor de la variable de iteración. Salida Hasta ahora hemos usado la instrucción echo para realizar salida a pantalla, esta instrucción es bastante limitada ya que no nos permite formatear la salida. En esta página veremos la instrucción printf que nos da mucha más potencia.

### Envío y recepción de datos


El lenguaje PHP nos proporciona una manera sencilla de manejar formularios, permitiéndonos de esta manera procesar la información que el usuario ha introducido. Al diseñar un formulario debemos indicar la página PHP que procesará el formulario, así como en método por el que se le pasará la información a la página.


    <html>
    <head>
    <title>Ejemplo de PHP</title>
    </head><body>
    <H1>Ejemplo de procesado de formularios</H1>
    
    Introduzca su nombre: 
    <form action="procesa.php" method="GET">
    <input type="text" name="nombre"/><br />
    <input type="submit" value="Enviar"/>
    </form>
    </body></html>
    
Al pulsar el botón Enviar el contenido de cuadro de texto es enviado a la página que indicamos en el atributo ACTION de la etiqueta FORM . En versiones anteriores a 4.2.0 PHP creaba una variable por cada elemento del FORM , esta variable creada tenía el mismo nombre que el cuadro de texto de la página anterior y el valor que habíamos introducido. Pero por razones de seguridad a partir de entonces para acceder a las variables del formulario hay que usar el array de parámetros $_POST[] o $_GET[] dependiendo del método usado para enviar los parámetros. En este ejemplo se ha creado una entrada en el array $_GET[] con el índice 'nombre' y con el valor que haya introducido el navegante.
    
    <html>
    <head>
    <title>Ejemplo de PHP</title>
    </head><body>
    <H1>Ejemplo de procesado de formularios</H1>
    El nombre que ha introducido es:<?php echo $_GET['nombre'] ?>
    <br>
     
    </body>
    </html>



## PHP - Programación orientada a objetos (POO) ##

Con los objetos y las clases ahora es posible agrupar información y acciones de una forma muy fácil.

#### Clase ####
Es un término genérico que, como su nombre lo indica, representa una clase, tipo o conjunto. En mi ejemplo como quiero representar los datos y acciones de una o más personas, voy a llamar a la clase Persona o Person (es buena práctica escribir el código en inglés) y esto es todo lo que hace falta para hacer el “molde” de mi clase:

     <?php

     class Person
     {

     }
     ?>     
     
Ahora todas las variables que necesito para representar el estado de cada persona (Person) las voy a declarar dentro de la clase y las voy a llamar propiedades:       

     <?php

     class Person
     {
         public $firstName = 'John';
         public $lastName = 'Doe';
         private $dateOfBirth = '1980-12-01;
     }

     ?>

Por otro lado todas las funciones o acciones que van a leer o interactuar con dichos datos (o propiedades) las voy también a convertir en métodos de la clase, tan sólo colocando dichas funciones dentro de la clase:

     <?php

     class Person
     {
         public $firstName = 'John';
         public $lastName = 'Doe';
         private $dateOfBirth = '1980-12-01;

         public function fullName()
         {
             return "{$this->firstName} {$this->lastName}";
         }

         public function age()
         {
             // Calculate age using $this->dateOfBirth...
             return $age;
         }
     }

     ?>

* Mientras que para trabajar con una variable usamos el signo de dólar $, para interactuar con una propiedad dentro de la clase usamos dólar, seguido de la palabra “this” y una flecha: $this->

#### Declarando un objeto

Mientras que la clase representa el concepto genérico, un objeto es la representación específica de una clase. Cuando tenemos una clase podemos declara uno o más objetos de dicha clase de esta forma:

     <?php

     $Roberto = new Person("Roberto", "Espeleta", "1987-04-18");

     ?>

* Nota que en este caso usamos el operador “new”, seguido del nombre de la clase y luego los argumentos que acepta el constructor de la clase.

#### Definiendo el constructor de una clase

Lo anterior no va a funcionar porque de hecho nuestra clase no tienen ningún constructor. Un constructor es un método mágico o especial que puede aceptar una serie de argumentos y nos permite inicializar el objeto, por ejemplo asignando las propiedades mínimas requeridas para que nuestro objeto pueda funcionar (por ejemplo tenga una identidad única), en nuestro ejemplo parecemos necesitar entonces el primer nombre, el apellido y la fecha de nacimiento:

    <?php
    
     class Person
     {
         public $firstName;
         public $lastName;
         public $dateOfBirth;
    
         public function __construct($firstName, $lastName, $dateOfBirth)
         {
             $this->firstName = $firstName;
             $this->lastName = $lastName;
             $this->dateOfBirth = $dateOfBirth;
         }
    
         public function fullName()
         {
           $fullName = $this->firstName.' '.$this->lastName;  
           return $fullName;
         }
    
         public function age()
         {
             // Calculate age using $this->dateOfBirth...
             return $age;
         }
     }
     
    ?> 

Los constructores de todas las clases de PHP son definidos con el mismo nombre (construct) y como puedes ver tal como las propiedades y el resto de los métodos, pueden ser públicos o privados, pero por ahora vamos a dejarlo como público. Con este código ya podemos proceder a instanciar nuestro objeto:


     <?php

     $Roberto = new Person("Roberto", "Espeleta", "1987-04-18");

     ?>
     
Ahora en algunas ocasiones queremos leer y/o modificar ciertas propiedades fuera de la clase. Si la propiedad es pública podemos lograrlo así:

     <?php

     $duilio = new Person("John", "Doe", "1980-12-01");

     $duilio->firstName = 'Duilio';

     ?>
     
     
## API REST ##

REST es cualquier interfaz entre sistemas que use HTTP para obtener datos o generar operaciones sobre esos datos en todos los formatos posibles, como XML y JSON.

### Características de REST ###

* Protocolo cliente/servidor sin estado: cada petición HTTP contiene toda la información necesaria para ejecutarla, lo que permite que ni cliente ni servidor necesiten recordar ningún estado previo para satisfacerla. Aunque esto es así, algunas aplicaciones HTTP incorporan memoria caché. Se configura lo que se conoce como protocolo cliente-caché-servidor sin estado: existe la posibilidad de definir algunas respuestas a peticiones HTTP concretas como cacheables, con el objetivo de que el cliente pueda ejecutar en un futuro la misma respuesta para peticiones idénticas. De todas formas, que exista la posibilidad no significa que sea lo más recomendable.
* Las operaciones más importantes relacionadas con los datos en cualquier sistema REST y la especificación HTTP son cuatro: POST (crear), GET (leer y consultar), PUT (editar) y DELETE (eliminar).
* Los objetos en REST siempre se manipulan a partir de la URI. Es la URI y ningún otro elemento el identificador único de cada recurso de ese sistema REST. La URI nos facilita acceder a la información para su modificación o borrado, o, por ejemplo, para compartir su ubicación exacta con terceros. 
* Interfaz uniforme: para la transferencia de datos en un sistema REST, este aplica acciones concretas (POST, GET, PUT y DELETE) sobre los recursos, siempre y cuando estén identificados con una URI. Esto facilita la existencia de una interfaz uniforme que sistematiza el proceso con la información.
* Sistema de capas: arquitectura jerárquica entre los componentes. Cada una de estas capas lleva a cabo una funcionalidad dentro del sistema REST.


### Ventajas que ofrece REST para el desarrollo  ###

* Separación entre el cliente y el servidor: el protocolo REST separa totalmente la interfaz de usuario del servidor y el almacenamiento de datos. Eso tiene algunas ventajas cuando se hacen desarrollos. Por ejemplo, mejora la portabilidad de la interfaz a otro tipo de plataformas, aumenta la escalabilidad de los proyectos y permite que los distintos componentes de los desarrollos se puedan evolucionar de forma independiente.
* Visibilidad, fiabilidad y escalabilidad. La separación entre cliente y servidor tiene una ventaja evidente y es que cualquier equipo de desarrollo puede escalar el producto sin excesivos problemas. Se puede migrar a otros servidores o realizar todo tipo de cambios en la base de datos, siempre y cuando los datos de cada una de las peticiones se envíen de forma correcta. Esta separación facilita tener en servidores distintos el front y el back y eso convierte a las aplicaciones en productos más flexibles a la hora de trabajar.
* La API REST siempre es independiente del tipo de plataformas o lenguajes: la API REST siempre se adapta al tipo de sintaxis o plataformas con las que se estén trabajando, lo que ofrece una gran libertad a la hora de cambiar o probar nuevos entornos dentro del desarrollo. Con una API REST se pueden tener servidores PHP, Java, Python o Node.js. Lo único que es indispensable es que las respuestas a las peticiones se hagan siempre en el lenguaje de intercambio de información usado, normalmente XML o JSON.


### Códigos de estado HTTP ###

Informan al navegador de algunas acciones que se van a realizar:

* 100 (Continue), el navegador puede continuar realizando su petición (se utiliza para indicar que la primera parte de la petición del navegador se ha recibido correctamente).
* 101 (Switching Protocols), el servidor acepta el cambio de protocolo propuesto por el navegador (puede ser por ejemplo un cambio de HTTP 1.0 a HTTP 1.1).
* 102 (Processing (WebDAV)), el servidor está procesando la petición del navegador pero todavía no ha terminado (esto evita que el navegador piense que la petición se ha perdido cuando no recibe ninguna respuesta).
* 103 (Checkpoint), se va a reanudar una petición POST o PUT que fue abortada previamente.
 

Indican que la petición del navegador se ha recibido, procesado y respondido correctamente:

* 200 (Ok), la petición del navegador se ha completado con éxito.
* 201 (Created), la petición del navegador se ha completado con éxito y como resultado, se ha creado un nuevo recurso (la respuesta incluye la URI de ese recurso).
* 202 (Accepted), la petición del navegador se ha aceptado y se está procesando en estos momentos, por lo que todavía no hay una respuesta (se utiliza por ejemplo cuando un proceso realiza una petición muy compleja a un servidor y no quiere estar horas esperando la respuesta).
* 203 (Non-Authoritative Information), la petición se ha completado con éxito, pero su contenido no se ha obtenido de la fuente originalmente solicitada sino de otro servidor.
* 204 (No Content), la petición se ha completado con éxito pero su respuesta no tiene ningún contenido (la respuesta sí que puede incluir información en sus cabeceras HTTP).
* 205 (Reset Content), la petición se ha completado con éxito, pero su respuesta no tiene contenidos y además, el navegador tiene que inicializar la página desde la que se realizó la petición (este código es útil por ejemplo para páginas con formularios cuyo contenido debe borrarse después de que el usuario lo envíe).
* 206 (Partial Content), La respuesta de esta petición sólo tiene parte de los contenidos, tal y como lo solicitó el propio navegador (se utiliza por ejemplo cuando se descarga un archivo muy grande en varias partes para acelerar la descarga).
* 207 (Multi-Status (WebDAV)), la respuesta consiste en un archivo XML que contiene en su interior varias respuestas diferentes (el número depende de las peticiones realizadas previamente por el navegador).
* 208 (Already Reported (WebDAV)), el listado de elementos DAV ya se notificó previamente, por lo que no se van a volver a listar.

Indican que el navegador debe realizar alguna acción adicional para que la petición se complete (como por ejemplo redirigirse a otra página):

* 300 (Multiple Choices), existe más de una variante para el recurso solicitado por el navegador (por ejemplo si la petición se corresponde con más de un archivo).
* 301 (Moved Permanently), el recurso solicitado por el navegador se encuentra en otro lugar y este cambio es permanente. El navegador es redirigido automáticamente a la nueva localización de ese recurso (este código es muy importante para tareas relacionadas con el SEO de los sitios web).
* 302 (Moved Temporarily), el recurso solicitado por el navegador se encuentra en otro lugar, aunque sólo por tiempo limitado. El navegador es redirigido automáticamente a la nueva localización de ese recurso.
* 303 (See Other), el recurso solicitado por el navegador se encuentra en otro lugar. El servidor no redirige automáticamente al navegador, pero le indica la nueva URI en la que se puede obtener el recurso.
* 304 (Not Modified), cuando el navegador pregunta si un recurso ha cambiado desde la última vez que se solicitó, el servidor responde con este código cuando el recurso no ha cambiado.
* 305 (Use Proxy), el recurso solicitado por el navegador debe obtenerse a través del proxy cuya dirección se indica en la cabecera Location de esta misma respuesta.
* 306 (Switch Proxy), este código se utilizaba en las versiones antiguas de HTTP pero ya no se usa (aunque está reservado para usos futuros).
* 307 (Temporary Redirect), el recurso solicitado por el navegador se puede obtener en otro lugar, pero sólo para esta petición. Las próximas peticiones pueden seguir utilizando la localización original del recurso.
* 308 (Permanent Redirect), el recurso solicitado por el navegador se encuentra en otro lugar y este cambio es permanente. A diferencia del código 301, no se permite cambiar el método HTTP para la nueva petición (así por ejemplo, si envías un formulario a un recurso que ha cambiado de lugar, todo seguirá funcionando bien).

Indican que se ha producido un error cuyo responsable es el navegador:

* 400 (Bad Request), el servidor no es capaz de entender la petición del navegador porque su sintaxis no es correcta.
* 401 (Unauthorized), el recurso solicitado por el navegador requiere de autenticación. La respuesta incluye una cabecera de tipo WWW-Authenticate para que el navegador pueda iniciar el proceso de autenticación.
* 402 (Payment Required), este código está reservado para usos futuros.
* 403 (Forbidden), la petición del navegador es correcta, pero el servidor no puede responder con el recurso solicitado porque se ha denegado el acceso.
* 404 (Not Found), el servidor no puede encontrar el recurso solicitado por el navegador y no es posible determinar si esta ausencia es temporal o permanente.
* 405 (Method Not Allowed), el navegador ha utilizado un método (GET, POST, etc.) no permitido por el servidor para obtener ese recurso.
* 406 (Not Acceptable), el recurso solicitado tiene un formato que en teoría no es aceptable por el navegador, según los valores que ha indicado en la cabecera Accept de la petición.
* 407 (Proxy Authentication Required), es muy similar al código 401, pero en este caso, el navegador debe autenticarse primero con un proxy.
* 408 (Request Timeout), el navegador ha tardado demasiado tiempo en realizar su petición y el servidor ya no espera esa petición. No obstante, el navegador puede realizar nuevas peticiones cuando quiera.
* 409 (Conflict), la petición del navegador no se ha podido completar porque se ha producido un conflicto con el recurso solicitado. El caso más habitual es el de las peticiones de tipo PUT que intentan modificar un recurso que a su vez ya ha sido modificado por otro lado.
* 410 (Gone), no es posible encontrar el recurso solicitado por el navegador y esta ausencia se considera permanente. Si existe alguna posibilidad de que el recurso vuelva a estar disponible, se debe utilizar el código 404.
* 411 (Length Required), el servidor rechaza la petición del navegador porque no incluye la cabecera Content-Length adecuada.
* 412 (Precondition Failed), el servidor no es capaz de cumplir con algunas de las condiciones impuestas por el navegador en su petición.
* 413 (Request Entity Too Large), la petición del navegador es demasiado grande y por ese motivo el servidor no la procesa.
* 414 (Request-URI Too Long), la URI de la petición del navegador es demasiado grande y por ese motivo el servidor no la procesa (esta condición se produce en muy raras ocasiones y casi siempre porque el navegador envía como GET una petición que debería ser POST).
* 415 (Unsupported Media Type), la petición del navegador tiene un formato que no entiende el servidor y por eso no se procesa.
* 416 (Requested Range Not Satisfiable), el navegador ha solicitado una porción inexistente de un recurso. Este error se produce cuando el navegador descarga por partes un archivo muy grande y calcula mal el tamaño de algún trozo.
* 417 (Expectation Failed), la petición del navegador no se procesa porque el servidor no es capaz de cumplir con los requerimientos de la cabecera Expect de la petición.
* 422 (Unprocessable Entity (WebDAV)), la petición del navegador tiene el formato correcto, pero sus contenidos tienen algún error semántico que impide al servidor responder.
* 423 (Locked (WebDAV)), el recurso solicitado por el navegador no se puede entregar porque está bloqueado.
* 424 (Failed Dependency (WebDAV)), la petición del navegador ha fallado debido al error de alguna petición anterior (por ejemplo una petición con el método PROPPATCH).
* 426 (Upgrade Required), el navegador debe cambiar a un protocolo diferente para realizar las peticiones (por ejemplo TLS/1.0).
* 428 (Precondition Required), el servidor requiere que la petición del navegador sea condicional (este tipo de peticiones evitan los problemas producidos al modificar con PUT un recurso que ha sido modificado por otra parte).
* 429 (Too Many Requests), el navegador ha realizado demasiadas peticiones en un determinado período de tiempo (se utiliza sobre todo para forzar los límites de consumo de recursos de las APIs).
* 431 (Request Header Fileds Too Large), el servidor no puede procesar la petición porque una de las cabeceras de la petición es demasiado grande. Este error también se produce cuando la suma del tamaño de todas las peticiones es demasiado grande.

Indican que se ha producido un error cuyo responsable es el servidor:

* 500 (Internal Server Error), la solicitud del navegador no se ha podido completar porque se ha producido un error inesperado en el servidor.
* 501 (Not Implemented), el servidor no soporta alguna funcionalidad necesaria para responder a la solicitud del navegador (como por ejemplo el método utilizado para la petición).
* 502 (Bad Gateway), el servidor está actuando de proxy o gateway y ha recibido una respuesta inválida del otro servidor, por lo que no puede responder adecuadamente a la petición del navegador.
* 503 (Service Unavailable), el servidor no puede responder a la petición del navegador porque está congestionado o está realizando tareas de mantenimiento.
* 504 (Gateway Timeout), , el servidor está actuando de proxy o gateway y no ha recibido a tiempo una respuesta del otro servidor, por lo que no puede responder adecuadamente a la petición del navegador.
* 505 (HTTP Version Not Supported), el servidor no soporta o no quiere soportar la versión del protocolo HTTP utilizada en la petición del navegador.
* 506 (Variant Also Negotiates), el servidor ha detectado una referencia circular al procesar la parte de la negociación del contenido de la petición.
* 507 (Insufficient Storage (WebDAV)), el servidor no puede crear o modificar el recurso solicitado porque no hay suficiente espacio de almacenamiento libre.
* 508 (Loop Detected (WebDAV)), la petición no se puede procesar porque el servidor ha encontrado un bucle infinito al intentar procesarla.
* 510 (Not Extended), la petición del navegador debe añadir más extensiones para que el servidor pueda procesarla.
* 511 (Network Authentication Required), el navegador debe autenticarse para poder realizar peticiones (se utiliza por ejemplo con los portales cautivos que te obligan a autenticarte antes de empezar a navegar).
 
## Slim Framework ##

Slim es un framework micro de PHP que nos ayudara a escribir rápidamente aplicaciones Web y API sencillas pero poderosas.


        <?php
        use \Psr\Http\Message\ServerRequestInterface as Request;
        use \Psr\Http\Message\ResponseInterface as Response;
        
        require 'vendor/autoload.php';
        
        $app = new \Slim\App;
        $app->get('/hello/{name}', function (Request $request, Response $response) {
            $name = $request->getAttribute('name');
            $response->getBody()->write("Hello, $name");
        
            return $response;
        });
        $app->run();


### Documentación ###

Slim es un framework micro de PHP que le ayuda a escribir rápidamente aplicaciones Web y API sencillas pero poderosas. En su núcleo, Slim es un despachador que recibe una solicitud HTTP, invoca una rutina de devolución de llamada adecuada y devuelve una respuesta HTTP. Eso es.

Slim es una herramienta ideal para crear API que consumen, reutilizan o publican datos. Slim es también una gran herramienta para el prototipado rápido. Heck, incluso puede crear aplicaciones web con todas las funciones con interfaces de usuario. Más importante aún, Slim es super rápido y tiene muy poco código. De hecho, usted puede leer y entender su código fuente en sólo una tarde!

https://www.slimframework.com/docs/


## PDO (PHP Data Objects) ##

Es una extensión que provee una capa de abstracción de acceso a datos para PHP 5, con lo cual se consigue hacer uso de las mismas funciones para hacer consultas y obtener datos de distintos manejadores de bases de datos.

Empezó a desarrollarse en 2003 tras unos meetings en LinuxTag.1​ Fue considerada experimental hasta PHP 5.0 (en el cual está disponible como una extensión PECL); a partir de PHP 5.1 se considera estable y la interfaz viene incluida por defecto.

Está implementada con tecnología orientada a objetos. La conexión a una base de datos se realiza creando una instancia de la clase base PDO. Algunos métodos son: prepare, execute, exec, beginTransaction, bindParam, commit.

Controladores
Los siguientes controladores actualmente implementan la interfaz PDO:

* PDO_DBLIB: FreeTDS / Microsoft SQL Server / Sybase
* PDO_FIREBIRD: Firebird / Interbase 6
* PDO_IBM: IBM DB2
* PDO_INFORMIX: IBM Informix Dynamic Server
* PDO_SQLSRV: Microsoft SQL Server
* PDO_MYSQL: MySQL 3.x/4.x/5.x
* PDO_OCI: Oracle Call Interface
* PDO_ODBC: ODBC v3 (IBM DB2, unixODBC y win32 ODBC)
* PDO_PGSQL: PostgreSQL
* PDO_SQLITE: SQLite 3 y SQLite 2

###  PDO - MYSQL ###


        <?php
            mysql_connect('localhost', 'username', 'password') or die ("Fallo.");
        ?>

Donde localhost, username y password serían el servidor, el nombre de usuario del servidor, y la contraseña del mismo, respectivamente. Esto está muy bien y es muy sencillo de recordar, pero con PDO se hace de otra manera, que quizá parezca algo más complicado al principio pero al fin y al cabo igual de intuitiva:


        <?php
            $conn = new PDO('mysql:host=localhost;dbname=mibasededatos', $usuario, $password);
        ?>

Aunque parezca raro realmente es muy sencillo: creamos con el “new” una instancia de clase PDO (lo que nos permitirá usar varias a la vez), definimos el tipo de base de datos con “mysql:”, el servidor “localhost” (en este caso), el nombre de la base de datos “dbname=mibasededatos”, y después usando variables (se puede hacer sin), el usuario y la contraseña del servidor de bases de datos.

Como puede haber errores y tenemos que controlarlos mediante el uso de try…catch(…){} de la siguiente manera:


        <?php
        try{
            $conn = new PDO('mysql:host=localhost;dbname=mibasededatos', $usuario, $password);
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }catch(PDOException $e){
            echo "ERROR: " . $e->getMessage();
        }
        ?>

De esta manera se conecta a la base de datos y en caso de que no pueda saca el error con un echo. Se puede ver que el try..catch hace la función del or die de la sintaxis habitual de PHP y MySQL.

        <?php
        $nombre = "Juan";
        try{
        	$conn = new PDO('mysql:host=localhost;dbname=basededatos', $usuario, $contra);
        	$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        
        	$sql = $conn->prepare('SELECT * FROM usuarios WHERE nombre = :Nombre');
        	$sql->execute(array('Nombre' => $nombre));
        	$resultado = $sql->fetchAll();
        
        	foreach ($resultado as $row) {
        		echo $row["Id"];
        	}
        }catch(PDOException $e){
        	echo "ERROR: " . $e->getMessage();
        }
        ?>
        
Primero hemos usado el método “prepare”, de la función conn, definida antes, y le hemos pasado la consulta como parámetro, cualquier consulta SQL, no hace falta que sea tan sencilla como esta, pero donde iría la variable “Nombre”, definida antes, he puesto “:Nombre”, y le he pasado eso como parámetro en un array dentro del execute en la línea siguiente (la línea 8). Más tarde he hecho un fetch para que me devuelva los datos de la consulta y para acabar, un foreach que imprime el resultado.

Cabe destacar el uso del fetch, ya que esta no es la única manera de usarlo en PDO. Si no se le pasa nada como parámetro, éste devolverá un array, pero también podemos definirlo para que devuelva un objeto, una instancia de clase, etc. Para esto solo tendremos que ponerle como parámetro lo siguiente:

* PDO::FETCH_ASSOC: Es el que viene por defecto, que devuelve un array. Es el que se usa en este ejemplo.
* PDO::FETCH_BOTH: que devuelve un array de otra manera (no voy a entrar muy en detalle, que el tutorial es para aprender lo básico).
* PDO::FETCH_BOUND
* PDO::FETCH_CLASS
* PDO::FETCH_OBJ


Finalmente, resta decir que para hacer otro tipo de consultas (INSERT, DELETE o  UPDATE), se hace de la misma manera.       
